Pro and Contra
==============

It helps to document the positive and negative sides of the considered topic. It emphasizes the differences and makes decisions easier.

In its simplest form this is a list of pro and contra facts of opinions side-by-side.

