Views
=====

Task dependency graph
---------------------

* Visualize all of the precedessor of the selected task.
* Shows only one level of successors.
* Graphical representation.

Table of tasks
--------------

* Tabular representation of tasks.
* The columns are optional.

Table of events
---------------

* List of corresponding events.
* Displays events of the selected task or events of any predecessor tasks.

Calendar view of events
-----------------------

* Displays the corresponding events in the calendar.

