Tracks
======

It has designed for continuous work with different kind of measurements. It makes possible to visualize the progress of long time work.

We can use this when

* practise something and it motivates for regularity,
* collect something and it shows the current number of the collected things,
* burn out long term work,
* collect some measurement for further statistical analysis.

