.. include:: <isonum.txt>

Tasks
=====

Attributes
----------

* **id**: Unique identifier
* **name**: The name of the task
* **description**: Detailed description about the task
* **type**: The type of the task
* **start**: The start date of the task
* **finish**: The finish date of the task
* **keywords**: Some keywords which are related to the task
* **owner**: The person, who is responsible for the fulfilment of the task
* **client**: The person, who waits the completion of the task


States
------

A task can be in one of the following states.

* **new**: The task has created but it waits time allocation.
* **in progress**: The task has started but the finish time has not known.
* **cancelled**: The task cancelled/rejected from some reasons.
* **successful**: The task has allocated which means the task has completed.

The state of the task does not stored. The state can be derived from timestamps.

.. csv-table::
   :header: "start", "finish", "state"

   NULL, NULL, new
   timestamp, NULL, in progress
   NULL, timestamp, cancelled
   timestamp, timestamp, successful


State transitions
-----------------

The following state transitions are valid:

* *new* |rarr| *new*, *in progress* |rarr| *new*, *cancelled* |rarr| *any*, *successful* |rarr| *any*

The other transitions from *new* and from *in progress* states are invalid, because the *in progress*, *cancelled* and *successful* states assume that all of the dependencies has fulfilled.


Edges
-----

The edges add ordering on the set of tasks. There are the following edge types.

* **dependency**: The starting of the second task depends on the first.
* **priority**: There is not direct dependency between the first and the second task but we prefer to finish the first task before the second one.

Let denote *first* |rarr| *second* a dependency between the first and the second tasks. The start date of the second task cannot be earlier than the finish date of the first. The creation date does not matter here. That is necessary for tracking the scheduling process.


Task types
----------

These types are not obligatory, but cover many common use cases.

* *administration*: do an administrative task
* *artistic*: an artistic design task
* *configuration*: configure something
* *consultation*: consultation with somebod
* *debit*: a debit to the given person
* *design*: design architecture
* *development*: software development task
* *documentation*: write documentation
* *freetime*: free time activity
* *lecture*: listen a lecture as a participant
* *mail*: write and send an email
* *plan*: fragment, order and schedule the task
* *private*: private time activity
* *project*: the head task of a project
* *quest*: try to answer a question somehow
* *requirement*: purchase or have something in
* *study*: study from a well-known subject
* *teach*: teach somebody
* *travel*: travel to the given place


Operations
----------

.. csv-table::
   :header: "operation", "parameters", "description"

   "createTask", "name, description", "Create a new task."
   "getTask", "taskId", "Get the task with the given identifier."
   "updateTask", "taskId, name, description", "Update the name and the description of the task."
   "destroyTask", "taskId", "Destroy the task given by task identifier."
   "setTimeInterval", "taskId, beginDate, endDate", "Set the time interval of the task."
   "cancelTask", "taskId", "Cancel the given task."
   "addEdge", "aTaskId, bTaskId, edgeType", "Add edge from task a to task b."
   "removeEdge", "aTaskId, bTaskId", "Remove the edge between the given tasks."
   "collectPredecessors", "taskId", "Collect the predecessors of the task."


Events
------

The events are necessary for analyzing the process of the project management and scheduling. It helps to log the important events in the task scheduling process. It considers the following task event types.

* *creation*: A new task has created.
* *allocation*: The start and finish time of the task has given.
* *start*: Only the start time of the task has given.
* *finish*: The finish time has given for making the task state successful.
* *cancellation*: The task has cancelled.
* *deletion*: The task has deleted from some reasons.

All of the events has a timestamp. The *allocation* event type is associated with a start and a finish date. It can be stored in a dedicated table (for example called ``allocations``).

.. note::

   The events are not the essential part of the system. It requires additional storages. The scheduling system remains usable without it.

