Strategist
==========

The *Imfea Strategist* is a task management system which based on a dependency graph. It helps to manage projects from various areas.

* Dependency, priority and version management
* Workflow visualization
* Time tracking
* Contact management
* Desicion making

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/tasks
   pages/contacts
   pages/views
   pages/decisions
   pages/procontra
   pages/tracks

